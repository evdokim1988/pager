# HSPager

[![Software License] (https://img.shields.io/badge/license-MIT-
brightgreen.svg?style=flat-square)] (LICENSE.md)

A library to split results into multiple pages

## Install

Via Composer

```bash
$ composer require heksweb/pager
```
## Usage

``` php
$obj =new HSPager\DirPager(
    new HSPager\PagesList(), 'photos', 3, 2
);
echo "<pre>";
print_r($obj->getitems());
echo "</pre>";
echo "<p>$obj</p>";
```

``` php
$obj =new HSPager\FilePager(
    new ISPager\ItemsRange(),'largetextfile.txt'
);
echo "<pre>";
print_r($obj->getitems());
echo "</pre>";
echo "<p>$obj</p>";
```

``` php
try {
    $pdo = new FOO(
        'mysql:host=localhost;dbname=test',
        'root',
        , ,
        [FOO: :ATTR_ERRMODE => FOO: :ERRMODE_EXCEPTION]
    );
    $obj = new HSPager\PdoPager(
        new HSPager\ItemsRange(), $pdo, 'table_name'
    );
    echo "<pre>";
    print_r($obj->getitems());
    echo "</pre>";
    echo "<p>$obj</p>";
}
catch (PDOException $e) {
    echo "Can't connect to database";
}
```

## License

The MIT License (MIT). Please see [License File) (https://github.com/dnoegel/php-xdg-base-dir/blob/master/LICENSE) for more information.