<?php
namespace HSPager;


abstract class View
{
    /**
     * @var Pager  [ Класс построничной навигации реализующий абстрактный класс Pager ]
     */
    protected $pager;

    /**
     * [ Функция возвращает отрисованную в HTML ссылку на страницу ]
     *
     * @param $title [ Название ссылки ]
     * @param int $current_page [ Номер страницы для ссылки ]
     * @return string
     */
    public function link($title,$current_page = 1)
    {
        return  "<a href='{$this->pager->getCurrentPagePath()}?" .
                "{$this->pager->getCounterParam()}={$current_page}" .
                "{$this->pager->getParameters()}'>{$title}</a>";
    }

    /**
     * [ Функция отрисовки построничной навигации ]
     *
     * @param Pager $pager
     * @return mixed
     */
    abstract public function render(Pager $pager);
}