<?php
namespace HSPager;

abstract class Pager
{
    /**
     * @var View
     */
    protected $view;

    /**
     * @var mixed
     */
    protected $parameters;

    /**
     * @var string
     */
    protected $counter_param;

    /**
     * @var int
     */
    protected $links_count;

    /**
     * @var int
     */
    protected $items_per_page;

    /**
     * Pager constructor.
     * @param View $view
     * @param int $items_per_page
     * @param int $links_count
     * @param null $get_params
     * @param string $counter_param
     */
    public function __construct(
        View $view,
        $items_per_page = 10,
        $links_count = 3,
        $get_params = null,
        $counter_param = 'page')
    {
        $this->view = $view;
        $this->parameters = $get_params;
        $this->counter_param = $counter_param;
        $this->items_per_page = $items_per_page;
        $this->links_count = $links_count;
    }

    /**
     * [ Возвращает общее количество элементов в разбиваемой на страницы коллекции ]
     *
     * @return mixed
     */
    abstract public function getItemsCount();

    /**
     * [ Возвращает массив с элементами текущей страницы ]
     *
     * @return mixed
     */
    abstract public function getItems();

    /**
     * [ Количество видимых ссылок слева и справа от текущей страницы ]
     *
     * @return int
     */
    public function getVisibleLinkCount()
    {
        return $this->links_count;
    }

    /**
     * [ Возвращает дополнительные GET-параметры, которые необходимо передавать по ссылкам ]
     *
     * @return mixed|null
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * [ Название GET-параметра, через который передается номер текущей страницы ]
     *
     * @return string
     */
    public function getCounterParam()
    {
        return $this->counter_param;
    }

    /**
     * [ Возвращает количество элементов на одной странице ]
     *
     * @return int
     */
    public function getItemsPerPage()
    {
        return $this->items_per_page;
    }

    /**
     * [ Возвращает путь к текущей странице ]
     *
     * @return mixed
     */
    public function getCurrentPagePath()
    {
        return $_SERVER['PHP_SELF'];
    }

    /**
     * [ Возвращает номер текущей страницы ]
     *
     * @return int
     */
    public function getCurrentPage()
    {
        if (isset($_GET[$this->getCounterParam()])) {
            return intval($_GET[$this->getCounterParam()]);
        } else {
            return 1;
        }
    }

    /**
     * [ Возвращает общее количество страниц ]
     *
     * @return int
     */
    public function getPagesCount()
    {
        //Количество позиций
        $total  = $this->getItemsCount();
        //Вычисляем количество страниц
        $result = (int)($total / $this->getItemsPerPage());
        if ((float)($total / $this->getItemsPerPage()) - $result != 0) $result++;

        return $result;
    }

    /**
     * @return mixed
     */
    public function render()
    {
        return $this->view->render($this);
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->render();
    }
}